$.fn.FocusPic = function(options) {
    // 默认值
    var defalut = {
        autoplay: false,
        interval: 6000,
        duration: 1000,
        pagination: false,
        title: false
    };
    // 初始化
    var options = $.extend(defalut, options);
    // 下面开始处理焦点图
    $(this).each(function() {
        var now = 0;
        var timer = null;
        var Ftitle = '';
        var Furl = '';
        var that = $(this);
        var slideLength = that.find('.focus-slide').length;
        //分页器
        if (options.pagination&&slideLength>1) {
            var pagination = that.find('.focus-pagination');
            var paginationHtml = '';
            for (i = 0; i < slideLength; i++) {
                paginationHtml += "<a></a>";
            }
            pagination.append(paginationHtml);
            pagination.find('a').on("click", function() {
                $(this).addClass('active').siblings('a').removeClass('active');
                show($(this).index());
            });
            pagination.find('a').eq(0).addClass('active');
        }
        var show=function (index) {
            // 切换
            that.find('.focus-slide').eq(index).siblings('.focus-slide').stop().animate({ 'zIndex': 1, 'opacity': 0 }, options.duration);
            that.find('.focus-slide').eq(index).stop().animate({ 'zIndex': 3, 'opacity': 1 }, options.duration);
            //title
            if (options.title) {
                Ftitle = that.find('.focus-slide').eq(index).find('img').attr('alt');
                Furl = that.find('.focus-slide').eq(index).find('img').attr('link');
                that.find('.focus-title span').text(Ftitle);
                that.find('.focus-title').attr('href',Furl);
            }
            //自动播放
            if (options.autoplay) {
                clearInterval(timer);
                timer = setInterval(function() {
                    if (index < slideLength - 1) {
                        index++;
                    } else {
                        index = 0;
                    }
                    that.find('.focus-pagination a').eq(index).addClass('active').siblings('a').removeClass('active');
                    show(index);
                }, options.interval);
            }
        };
        // 初始化
        show(0);
    });

};
